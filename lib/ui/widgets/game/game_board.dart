import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sortgame/cubit/activity/activity_cubit.dart';
import 'package:sortgame/models/activity/activity.dart';
import 'package:sortgame/ui/widgets/game/game_question.dart';

class GameBoardWidget extends StatelessWidget {
  const GameBoardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocBuilder<ActivityCubit, ActivityState>(
        builder: (BuildContext context, ActivityState activityState) {
          final Activity currentActivity = activityState.currentActivity;

          return !currentActivity.isFinished
              ? const GameQuestionWidget()
              : const SizedBox.shrink();
        },
      ),
    );
  }
}
