import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sortgame/config/application_config.dart';

import 'package:sortgame/data/fetch_data_helper.dart';
import 'package:sortgame/models/data/game_item.dart';

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = false,
    this.isStarted = false,
    this.isFinished = false,
    this.animationInProgress = false,

    // Base data
    required this.items,

    // Game data
    this.position = 1,
    this.score = 0,
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;
  bool isStarted;
  bool isFinished;
  bool animationInProgress;

  // Base data
  final List<GameItem> items;

  // Game data
  int position;
  int score;

  factory Activity.createEmpty() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      items: [],
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    final List<GameItem> items = FetchDataHelper().getItems(newActivitySettings);

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // State
      isRunning: true,
      // Base data
      items: items,
    );
  }

  bool get canBeResumed => isStarted && !isFinished;

  bool get gameWon => isRunning && isStarted && isFinished;

  void increaseScore(int? count) {
    score += (count ?? 0);
  }

  void increasePosition() {
    position += 1;
  }

  void updateGameIsRunning(bool gameIsRunning) {
    isRunning = gameIsRunning;
  }

  void updateGameIsFinished(bool gameIsFinished) {
    isFinished = gameIsFinished;
  }

  GameItem getCurrentGameItem() {
    return items[position - 1];
  }

  void dump() {
    printlog('');
    printlog('## Current game dump:');
    printlog('');
    printlog('$Activity:');
    printlog('  Settings');
    activitySettings.dump();
    printlog('  State');
    printlog('    isRunning: $isRunning');
    printlog('    isStarted: $isStarted');
    printlog('    isFinished: $isFinished');
    printlog('    animationInProgress: $animationInProgress');
    printlog('  Base data');
    printlog('    items: $items');
    printlog('  Game data');
    printlog('    position: $position');
    printlog('    score: $score');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isRunning': isRunning,
      'isStarted': isStarted,
      'isFinished': isFinished,
      'animationInProgress': animationInProgress,
      // Base data
      'items': items,
      // Game data
      'position': position,
      'score': score,
    };
  }
}
