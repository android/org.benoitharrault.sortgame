import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sortgame/cubit/activity/activity_cubit.dart';

class ScoreIndicator extends StatelessWidget {
  const ScoreIndicator({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        const Color baseColor = Color.fromARGB(255, 218, 218, 218);
        final Color outlineColor = baseColor.darken();

        return OutlinedText(
          text: '${activityState.currentActivity.score}',
          fontSize: 70,
          textColor: baseColor,
          outlineColor: outlineColor,
        );
      },
    );
  }
}
