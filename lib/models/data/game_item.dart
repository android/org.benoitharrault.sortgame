import 'package:sortgame/models/data/category.dart';
import 'package:sortgame/models/data/item.dart';

class GameItem {
  final Item item;
  final List<Category> isCategory;
  final List<Category> isNotCategory;

  GameItem({
    required this.item,
    required this.isCategory,
    required this.isNotCategory,
  });

  @override
  String toString() {
    return '$GameItem(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'item': item,
      'isCategory': isCategory,
      'isNotCategory': isNotCategory,
    };
  }
}
