import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sortgame/cubit/activity/activity_cubit.dart';
import 'package:sortgame/models/data/category.dart';
import 'package:sortgame/models/data/game_item.dart';

class GameButtonsYesNo extends StatelessWidget {
  const GameButtonsYesNo({super.key, required this.gameItem});

  final GameItem gameItem;

  @override
  Widget build(BuildContext context) {
    final ActivityCubit activityCubit = BlocProvider.of<ActivityCubit>(context);

    final bool pickInIsCategory = Random().nextBool();

    final List<Category> categories =
        pickInIsCategory ? gameItem.isCategory : gameItem.isNotCategory;

    categories.shuffle();
    final Category category = categories.first;

    return Column(
      children: [
        Text(
          category.text,
          style: TextStyle(
            color: Theme.of(context).colorScheme.onSurface,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            IconButton(
              color: Theme.of(context).colorScheme.onSurface,
              iconSize: 80,
              onPressed: () {
                if (pickInIsCategory) {
                  activityCubit.increaseScore(1);
                }
                activityCubit.increasePosition();
              },
              icon: const Icon(UniconsLine.thumbs_up),
            ),
            Text(
              category.emoji,
              style: TextStyle(
                color: Theme.of(context).colorScheme.onSurface,
                fontSize: 40,
                fontWeight: FontWeight.bold,
              ),
            ),
            IconButton(
              color: Theme.of(context).colorScheme.onSurface,
              iconSize: 80,
              onPressed: () {
                if (!pickInIsCategory) {
                  activityCubit.increaseScore(1);
                }
                activityCubit.increasePosition();
              },
              icon: const Icon(UniconsLine.thumbs_down),
            ),
          ],
        )
      ],
    );
  }
}
