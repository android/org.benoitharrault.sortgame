class GameData {
  static const Map<String, dynamic> data = {
    "categories": [
      "animal",
      "artificiel",
      "gazeux",
      "inerte",
      "liquide",
      "naturel",
      "solide",
      "végétal"
    ],
    "resources": {
      "categories": {
        "animal": "🐈",
        "artificiel": "✨",
        "gazeux": "☁️",
        "inerte": "🪨",
        "liquide": "💧",
        "naturel": "🏞️",
        "solide": "💎",
        "végétal": "🌿"
      }
    },
    "exclusions": [
      ["liquide", "solide", "gazeux"],
      ["inerte", "animal", "végétal"],
      ["naturel", "artificiel"]
    ],
    "themes": {
      "tout": [],
      "état": ["liquide", "solide", "gazeux"],
      "genre": ["inerte", "animal", "végétal"],
      "vivant": ["animal", "végétal"]
    },
    "items": [
      "abeille",
      "aiguille",
      "allumette",
      "ambulance",
      "ampoule",
      "ananas",
      "aquarium",
      "araignée",
      "arbre",
      "armoire",
      "arrosoir",
      "asperge",
      "aspirateur",
      "aubergine",
      "autruche",
      "avion",
      "balai",
      "balançoire",
      "baleine",
      "ballon",
      "banane",
      "bassine",
      "bateau",
      "bavoir",
      "berceau",
      "beurre",
      "biberon",
      "biscuit",
      "bonbon",
      "bonnet",
      "botte",
      "bouchon",
      "bougie",
      "bouquet",
      "bouteille",
      "brosse",
      "brouette",
      "bébé",
      "bûche",
      "cactus",
      "cafetière",
      "café",
      "cahier",
      "camion",
      "canard",
      "carotte",
      "cartable",
      "casque",
      "casquette",
      "casserole",
      "castor",
      "cerf-volant",
      "chaise",
      "champignon",
      "chateau",
      "chaussette",
      "chaussure",
      "cheminée",
      "chemise",
      "cheval",
      "cintre",
      "ciseaux",
      "citron",
      "citrouille",
      "coccinelle",
      "cocotte-minute",
      "collier",
      "concombre",
      "confiture",
      "coq",
      "coquelicot",
      "corbeau",
      "corde",
      "cornichon",
      "crabe",
      "crayon",
      "crocodile",
      "culotte",
      "dauphin",
      "dinosaure",
      "dromadaire",
      "escalier",
      "escargot",
      "fantôme",
      "fenêtre",
      "fleur",
      "flûte",
      "fourchette",
      "fourmi",
      "fraise",
      "framboise",
      "fromage",
      "fusée",
      "gant",
      "gilet",
      "gomme",
      "gourde",
      "grenouille",
      "grille-pain",
      "guitare",
      "guêpe",
      "harmonica",
      "hippocampe",
      "hippopotame",
      "hélicoptère",
      "jambon",
      "journal",
      "jupe",
      "kangourou",
      "kayak",
      "kiwi",
      "lait",
      "lampe",
      "lapin",
      "lave-linge",
      "libellule",
      "licorne",
      "lion",
      "lit",
      "litière",
      "livre",
      "loup",
      "lune",
      "lunettes",
      "maison",
      "marteau",
      "micro",
      "moto",
      "mouche",
      "mouton",
      "mésange",
      "métro",
      "noeud",
      "noisette",
      "oeuf",
      "oie",
      "orage",
      "orchestre",
      "ordinateur",
      "oreiller",
      "ours",
      "pain",
      "panda",
      "pantalon",
      "papillon",
      "paquebot",
      "parapluie",
      "passoire",
      "peigne",
      "pelle",
      "pelote",
      "peluche",
      "pendule",
      "perceuse",
      "perle",
      "perroquet",
      "piano",
      "pigeon",
      "piscine",
      "pivert",
      "pizza",
      "plage",
      "poire",
      "poisson",
      "pomme",
      "pomme-de-terre",
      "pompier",
      "poubelle",
      "poulet",
      "poupée",
      "poussette",
      "poussin",
      "puzzle",
      "pyjama",
      "pâtes",
      "péniche",
      "pêche",
      "radis",
      "raquette",
      "renard",
      "requin",
      "rhinocéros",
      "rivière",
      "robinet",
      "rose",
      "râteau",
      "réveil",
      "sac",
      "sac à dos",
      "salade",
      "sandale",
      "sandwich",
      "sanglier",
      "saucisse",
      "saucisson",
      "sauterelle",
      "savon",
      "scie",
      "seau",
      "serpent",
      "singe",
      "soleil",
      "soupe",
      "sous-marin",
      "stade",
      "statue",
      "stylo",
      "sucre",
      "tabouret",
      "tache",
      "taille-crayon",
      "tapis",
      "tartine",
      "tasse",
      "thermomètre",
      "tire-bouchon",
      "tiroir",
      "toboggan",
      "tondeuse",
      "torchon",
      "tortue",
      "tournevis",
      "tracteur",
      "train",
      "trompette",
      "tulipe",
      "téléphone",
      "télévision",
      "vache",
      "valise",
      "vase",
      "violon",
      "voilier",
      "voiture",
      "vélo",
      "yaourt",
      "zèbre",
      "échelle",
      "écureuil",
      "éléphant",
      "étoile",
      "évier"
    ],
    "mapping": {
      "items": {
        "abeille": {
          "is": ["animal", "naturel", "solide"],
          "isnot": ["artificiel", "gazeux", "inerte", "liquide", "végétal"],
          "na": []
        },
        "aiguille": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "allumette": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "ambulance": {
          "is": ["inerte"],
          "isnot": ["animal", "gazeux", "végétal"],
          "na": []
        },
        "ampoule": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "ananas": {
          "is": ["végétal"],
          "isnot": ["animal", "inerte", "liquide"],
          "na": []
        },
        "aquarium": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "araignée": {
          "is": ["animal"],
          "isnot": ["inerte", "liquide", "végétal"],
          "na": []
        },
        "arbre": {
          "is": ["naturel", "solide", "végétal"],
          "isnot": ["animal", "artificiel", "gazeux", "inerte", "liquide"],
          "na": []
        },
        "armoire": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "arrosoir": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "asperge": {
          "is": ["végétal"],
          "isnot": ["animal", "inerte", "solide"],
          "na": []
        },
        "aspirateur": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "aubergine": {
          "is": ["végétal"],
          "isnot": ["animal", "inerte"],
          "na": []
        },
        "autruche": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "gazeux", "inerte", "végétal"],
          "na": []
        },
        "avion": {
          "is": ["inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "végétal"],
          "na": []
        },
        "balai": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "balançoire": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "baleine": {
          "is": ["animal", "naturel", "solide"],
          "isnot": ["artificiel", "gazeux", "inerte", "liquide", "végétal"],
          "na": []
        },
        "ballon": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "banane": {
          "is": ["naturel", "solide", "végétal"],
          "isnot": ["animal", "artificiel", "gazeux", "inerte", "liquide"],
          "na": []
        },
        "bassine": {
          "is": ["inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "végétal"],
          "na": []
        },
        "bateau": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "bavoir": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "berceau": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "beurre": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "biberon": {
          "is": ["inerte"],
          "isnot": ["animal", "liquide", "végétal"],
          "na": []
        },
        "biscuit": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "bonbon": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "bonnet": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "botte": {
          "is": ["inerte"],
          "isnot": ["animal", "gazeux", "végétal"],
          "na": []
        },
        "bouchon": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "bougie": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "bouquet": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "bouteille": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "brosse": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "brouette": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "bébé": {
          "is": ["animal"],
          "isnot": ["inerte", "végétal"],
          "na": []
        },
        "bûche": {
          "is": ["végétal"],
          "isnot": ["animal", "inerte"],
          "na": []
        },
        "cactus": {
          "is": ["solide", "végétal"],
          "isnot": ["animal", "gazeux", "inerte", "liquide"],
          "na": []
        },
        "cafetière": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "café": {
          "is": ["liquide", "végétal"],
          "isnot": ["animal", "gazeux", "inerte", "solide"],
          "na": []
        },
        "cahier": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "camion": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "canard": {
          "is": ["animal", "naturel", "solide"],
          "isnot": ["artificiel", "gazeux", "inerte", "liquide", "végétal"],
          "na": []
        },
        "carotte": {
          "is": ["végétal"],
          "isnot": ["animal", "inerte", "liquide"],
          "na": []
        },
        "cartable": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "casque": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "casquette": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "casserole": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "castor": {
          "is": ["animal", "naturel"],
          "isnot": ["inerte", "végétal"],
          "na": []
        },
        "cerf-volant": {
          "is": ["inerte"],
          "isnot": ["animal", "gazeux", "végétal"],
          "na": []
        },
        "chaise": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "champignon": {
          "is": ["végétal"],
          "isnot": ["animal", "inerte"],
          "na": []
        },
        "chateau": {
          "is": ["inerte"],
          "isnot": ["animal", "gazeux", "végétal"],
          "na": []
        },
        "chaussette": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "chaussure": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "cheminée": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "gazeux", "naturel", "végétal"],
          "na": []
        },
        "chemise": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "cheval": {
          "is": ["animal"],
          "isnot": ["inerte", "végétal"],
          "na": []
        },
        "cintre": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "ciseaux": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "citron": {
          "is": ["végétal"],
          "isnot": ["animal", "inerte"],
          "na": []
        },
        "citrouille": {
          "is": ["naturel", "végétal"],
          "isnot": ["animal", "artificiel", "gazeux", "inerte"],
          "na": []
        },
        "coccinelle": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "gazeux", "inerte", "végétal"],
          "na": ["liquide", "solide"]
        },
        "cocotte-minute": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "collier": {
          "is": ["inerte"],
          "isnot": ["animal", "gazeux", "végétal"],
          "na": []
        },
        "concombre": {
          "is": ["végétal"],
          "isnot": ["animal", "inerte"],
          "na": []
        },
        "confiture": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "coq": {
          "is": ["animal", "naturel", "solide"],
          "isnot": ["artificiel", "gazeux", "inerte", "liquide", "végétal"],
          "na": []
        },
        "coquelicot": {
          "is": ["solide", "végétal"],
          "isnot": ["animal", "gazeux", "inerte", "liquide"],
          "na": []
        },
        "corbeau": {
          "is": ["animal"],
          "isnot": ["inerte", "végétal"],
          "na": []
        },
        "corde": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "cornichon": {
          "is": ["naturel", "végétal"],
          "isnot": ["animal", "artificiel", "gazeux", "inerte"],
          "na": []
        },
        "crabe": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "inerte", "végétal"],
          "na": []
        },
        "crayon": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "crocodile": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "gazeux", "inerte", "végétal"],
          "na": ["liquide", "solide"]
        },
        "culotte": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "dauphin": {
          "is": ["animal"],
          "isnot": ["inerte", "végétal"],
          "na": []
        },
        "dinosaure": {
          "is": ["animal"],
          "isnot": ["gazeux", "inerte", "liquide", "végétal"],
          "na": []
        },
        "dromadaire": {
          "is": ["animal", "naturel", "solide"],
          "isnot": ["artificiel", "gazeux", "inerte", "liquide", "végétal"],
          "na": []
        },
        "escalier": {
          "is": ["inerte"],
          "isnot": ["animal", "gazeux", "végétal"],
          "na": []
        },
        "escargot": {
          "is": ["animal"],
          "isnot": ["inerte", "liquide", "végétal"],
          "na": []
        },
        "fantôme": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": ["naturel"]
        },
        "fenêtre": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "fleur": {
          "is": ["naturel", "végétal"],
          "isnot": ["animal", "artificiel", "inerte"],
          "na": []
        },
        "flûte": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "fourchette": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "fourmi": {
          "is": ["animal", "naturel", "solide"],
          "isnot": ["artificiel", "gazeux", "inerte", "liquide", "végétal"],
          "na": []
        },
        "fraise": {
          "is": ["végétal"],
          "isnot": ["animal", "inerte"],
          "na": []
        },
        "framboise": {
          "is": ["naturel", "végétal"],
          "isnot": ["animal", "artificiel", "inerte"],
          "na": []
        },
        "fromage": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "liquide", "naturel", "végétal"],
          "na": []
        },
        "fusée": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "gant": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "gilet": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "gomme": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "gourde": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "grenouille": {
          "is": ["animal"],
          "isnot": ["inerte", "végétal"],
          "na": []
        },
        "grille-pain": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "guitare": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "guêpe": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "inerte", "végétal"],
          "na": ["gazeux", "liquide", "solide"]
        },
        "harmonica": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "hippocampe": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "inerte", "végétal"],
          "na": []
        },
        "hippopotame": {
          "is": ["animal"],
          "isnot": ["inerte", "végétal"],
          "na": []
        },
        "hélicoptère": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "jambon": {
          "is": ["inerte"],
          "isnot": ["animal", "liquide", "végétal"],
          "na": []
        },
        "journal": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "jupe": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "kangourou": {
          "is": ["animal", "naturel", "solide"],
          "isnot": ["artificiel", "gazeux", "inerte", "liquide", "végétal"],
          "na": []
        },
        "kayak": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "kiwi": {
          "is": ["végétal"],
          "isnot": ["animal", "inerte"],
          "na": []
        },
        "lait": {
          "is": ["inerte"],
          "isnot": ["animal", "solide", "végétal"],
          "na": []
        },
        "lampe": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "lapin": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "inerte", "végétal"],
          "na": []
        },
        "lave-linge": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "libellule": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "inerte", "végétal"],
          "na": []
        },
        "licorne": {
          "is": ["animal", "naturel", "solide"],
          "isnot": ["artificiel", "gazeux", "inerte", "liquide", "végétal"],
          "na": []
        },
        "lion": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "inerte", "végétal"],
          "na": []
        },
        "lit": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "litière": {
          "is": ["inerte"],
          "isnot": ["animal", "gazeux", "végétal"],
          "na": []
        },
        "livre": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "loup": {
          "is": ["animal"],
          "isnot": ["inerte", "végétal"],
          "na": []
        },
        "lune": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "lunettes": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "maison": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "marteau": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "micro": {
          "is": ["inerte"],
          "isnot": ["animal", "gazeux", "liquide", "végétal"],
          "na": []
        },
        "moto": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "mouche": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "gazeux", "inerte", "végétal"],
          "na": ["liquide", "solide"]
        },
        "mouton": {
          "is": ["animal"],
          "isnot": ["inerte", "végétal"],
          "na": []
        },
        "mésange": {
          "is": ["animal"],
          "isnot": ["gazeux", "inerte", "végétal"],
          "na": []
        },
        "métro": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "noeud": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": ["solide"]
        },
        "noisette": {
          "is": ["naturel", "végétal"],
          "isnot": ["animal", "artificiel", "gazeux", "inerte"],
          "na": []
        },
        "oeuf": {
          "is": ["animal"],
          "isnot": ["gazeux", "inerte", "végétal"],
          "na": []
        },
        "oie": {
          "is": ["animal"],
          "isnot": ["inerte", "végétal"],
          "na": []
        },
        "orage": {
          "is": ["inerte", "naturel"],
          "isnot": ["animal", "artificiel", "gazeux", "liquide", "végétal"],
          "na": ["solide"]
        },
        "orchestre": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "ordinateur": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "oreiller": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "ours": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "gazeux", "inerte", "végétal"],
          "na": ["liquide", "solide"]
        },
        "pain": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "panda": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "inerte", "végétal"],
          "na": []
        },
        "pantalon": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "papillon": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "gazeux", "inerte", "liquide", "végétal"],
          "na": ["solide"]
        },
        "paquebot": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "parapluie": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "liquide", "naturel", "végétal"],
          "na": []
        },
        "passoire": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "peigne": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "pelle": {
          "is": ["inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "végétal"],
          "na": []
        },
        "pelote": {
          "is": ["inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "végétal"],
          "na": []
        },
        "peluche": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "pendule": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "perceuse": {
          "is": ["inerte"],
          "isnot": ["animal", "liquide", "végétal"],
          "na": []
        },
        "perle": {
          "is": ["inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "végétal"],
          "na": []
        },
        "perroquet": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "inerte", "végétal"],
          "na": []
        },
        "piano": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "pigeon": {
          "is": ["animal"],
          "isnot": ["inerte", "végétal"],
          "na": []
        },
        "piscine": {
          "is": ["inerte"],
          "isnot": ["animal", "gazeux", "végétal"],
          "na": []
        },
        "pivert": {
          "is": ["animal", "solide"],
          "isnot": ["gazeux", "inerte", "liquide", "végétal"],
          "na": []
        },
        "pizza": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "plage": {
          "is": ["inerte", "naturel"],
          "isnot": ["animal", "artificiel", "végétal"],
          "na": []
        },
        "poire": {
          "is": ["végétal"],
          "isnot": ["animal", "inerte"],
          "na": []
        },
        "poisson": {
          "is": ["animal"],
          "isnot": ["inerte", "végétal"],
          "na": []
        },
        "pomme": {
          "is": ["naturel", "solide", "végétal"],
          "isnot": ["animal", "artificiel", "gazeux", "inerte", "liquide"],
          "na": []
        },
        "pomme-de-terre": {
          "is": ["végétal"],
          "isnot": ["animal", "inerte"],
          "na": []
        },
        "pompier": {
          "is": [],
          "isnot": ["animal", "gazeux", "liquide"],
          "na": ["inerte", "végétal"]
        },
        "poubelle": {
          "is": ["inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "végétal"],
          "na": []
        },
        "poulet": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "gazeux", "inerte", "végétal"],
          "na": ["liquide", "solide"]
        },
        "poupée": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "poussette": {
          "is": ["inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "végétal"],
          "na": []
        },
        "poussin": {
          "is": ["animal"],
          "isnot": ["inerte", "végétal"],
          "na": []
        },
        "puzzle": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "pyjama": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "pâtes": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "péniche": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "pêche": {
          "is": ["naturel", "solide", "végétal"],
          "isnot": ["animal", "artificiel", "gazeux", "inerte", "liquide"],
          "na": []
        },
        "radis": {
          "is": ["végétal"],
          "isnot": ["animal", "inerte", "liquide"],
          "na": []
        },
        "raquette": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "gazeux", "naturel", "végétal"],
          "na": []
        },
        "renard": {
          "is": ["animal"],
          "isnot": ["gazeux", "inerte", "végétal"],
          "na": []
        },
        "requin": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "gazeux", "inerte", "liquide", "végétal"],
          "na": ["solide"]
        },
        "rhinocéros": {
          "is": ["animal", "solide"],
          "isnot": ["gazeux", "inerte", "liquide", "végétal"],
          "na": []
        },
        "rivière": {
          "is": ["naturel"],
          "isnot": ["animal", "artificiel"],
          "na": ["inerte", "végétal"]
        },
        "robinet": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "rose": {
          "is": ["naturel", "solide", "végétal"],
          "isnot": ["animal", "artificiel", "gazeux", "inerte", "liquide"],
          "na": []
        },
        "râteau": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "réveil": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "sac": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "sac à dos": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "salade": {
          "is": ["naturel", "végétal"],
          "isnot": ["animal", "artificiel", "inerte"],
          "na": []
        },
        "sandale": {
          "is": ["inerte"],
          "isnot": ["animal", "liquide", "végétal"],
          "na": []
        },
        "sandwich": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "sanglier": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "gazeux", "inerte", "liquide", "végétal"],
          "na": ["solide"]
        },
        "saucisse": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "saucisson": {
          "is": ["inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "végétal"],
          "na": []
        },
        "sauterelle": {
          "is": ["animal"],
          "isnot": ["inerte", "végétal"],
          "na": []
        },
        "savon": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "gazeux", "naturel", "végétal"],
          "na": ["liquide", "solide"]
        },
        "scie": {
          "is": ["inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "végétal"],
          "na": []
        },
        "seau": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "serpent": {
          "is": ["animal"],
          "isnot": ["inerte", "liquide", "végétal"],
          "na": []
        },
        "singe": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "gazeux", "inerte", "liquide", "végétal"],
          "na": ["solide"]
        },
        "soleil": {
          "is": ["inerte", "naturel"],
          "isnot": ["animal", "artificiel", "liquide", "végétal"],
          "na": []
        },
        "soupe": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "sous-marin": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "stade": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "statue": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "stylo": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "sucre": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "tabouret": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "tache": {
          "is": ["inerte"],
          "isnot": ["animal", "gazeux", "végétal"],
          "na": ["artificiel", "liquide", "solide"]
        },
        "taille-crayon": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "tapis": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "tartine": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "tasse": {
          "is": ["inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "végétal"],
          "na": []
        },
        "thermomètre": {
          "is": ["inerte"],
          "isnot": ["animal", "gazeux", "liquide", "végétal"],
          "na": []
        },
        "tire-bouchon": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "tiroir": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "toboggan": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "tondeuse": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "torchon": {
          "is": ["inerte"],
          "isnot": ["animal", "gazeux", "végétal"],
          "na": []
        },
        "tortue": {
          "is": ["animal", "naturel", "solide"],
          "isnot": ["artificiel", "gazeux", "inerte", "liquide", "végétal"],
          "na": []
        },
        "tournevis": {
          "is": ["inerte"],
          "isnot": ["animal", "gazeux", "végétal"],
          "na": []
        },
        "tracteur": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "train": {
          "is": ["inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "végétal"],
          "na": []
        },
        "trompette": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "tulipe": {
          "is": ["solide", "végétal"],
          "isnot": ["animal", "gazeux", "inerte", "liquide"],
          "na": []
        },
        "téléphone": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "télévision": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "vache": {
          "is": ["animal", "naturel", "solide"],
          "isnot": ["artificiel", "gazeux", "inerte", "liquide", "végétal"],
          "na": []
        },
        "valise": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "vase": {
          "is": ["inerte"],
          "isnot": ["animal", "gazeux", "végétal"],
          "na": []
        },
        "violon": {
          "is": ["inerte"],
          "isnot": ["animal", "liquide", "végétal"],
          "na": []
        },
        "voilier": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "voiture": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        },
        "vélo": {
          "is": ["artificiel", "inerte", "solide"],
          "isnot": ["animal", "gazeux", "liquide", "naturel", "végétal"],
          "na": []
        },
        "yaourt": {
          "is": ["artificiel"],
          "isnot": ["animal", "gazeux", "naturel", "végétal"],
          "na": ["inerte"]
        },
        "zèbre": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "gazeux", "inerte", "liquide", "végétal"],
          "na": ["solide"]
        },
        "échelle": {
          "is": ["artificiel", "inerte"],
          "isnot": ["animal", "naturel", "végétal"],
          "na": []
        },
        "écureuil": {
          "is": ["animal"],
          "isnot": ["inerte", "végétal"],
          "na": []
        },
        "éléphant": {
          "is": ["animal", "naturel"],
          "isnot": ["artificiel", "inerte", "végétal"],
          "na": []
        },
        "étoile": {
          "is": ["inerte", "naturel", "solide"],
          "isnot": ["animal", "artificiel", "gazeux", "liquide", "végétal"],
          "na": []
        },
        "évier": {
          "is": ["inerte"],
          "isnot": ["animal", "végétal"],
          "na": []
        }
      }
    }
  };
}
