import 'package:flutter/material.dart';

import 'package:sortgame/ui/widgets/indicators/indicator_position.dart';
import 'package:sortgame/ui/widgets/indicators/indicator_score.dart';

class GameTopWidget extends StatelessWidget {
  const GameTopWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        PositionIndicator(),
        ScoreIndicator(),
      ],
    );
  }
}
