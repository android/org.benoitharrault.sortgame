import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sortgame/cubit/activity/activity_cubit.dart';

import 'package:sortgame/ui/pages/game.dart';

import 'package:sortgame/data/fetch_data_helper.dart';

class ApplicationConfig {
  // activity parameter: items count
  static const String parameterCodeItemsCount = 'itemsCount';
  static const String itemsCountValueLow = '5';
  static const String itemsCountValueMedium = '10';
  static const String itemsCountValueHigh = '15';
  static const String itemsCountValueVeryHigh = '20';

  // activity parameter: theme code
  static const String parameterCodeThemeCode = 'themeCode';

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexGame = 1;

  static List<ApplicationSettingsParameterItemValue> getThemeCodes() {
    List<ApplicationSettingsParameterItemValue> list = [];

    FetchDataHelper().getThemesCodes().forEach((String themeCode) {
      list.add(ApplicationSettingsParameterItemValue(
        value: themeCode,
        color: Color((themeCode.hashCode * 0xFFFFFF).toInt()).withOpacity(1.0),
        isDefault: true,
      ));
    });

    return list;
  }

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'SortGame',
    activitySettings: [
      // items count
      ApplicationSettingsParameter(
        code: parameterCodeItemsCount,
        values: [
          ApplicationSettingsParameterItemValue(
            value: itemsCountValueLow,
            color: Colors.green,
          ),
          ApplicationSettingsParameterItemValue(
            value: itemsCountValueMedium,
            color: Colors.orange,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: itemsCountValueHigh,
            color: Colors.red,
          ),
          ApplicationSettingsParameterItemValue(
            value: itemsCountValueVeryHigh,
            color: Colors.purple,
          ),
        ],
      ),

      // theme code
      ApplicationSettingsParameter(
        code: parameterCodeThemeCode,
        values: getThemeCodes(),
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
      },
    ),
  );
}
