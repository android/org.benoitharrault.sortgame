import 'package:sortgame/models/data/category.dart';

class GameTheme {
  final String code;
  final List<Category> categories;

  GameTheme({
    required this.code,
    required this.categories,
  });

  @override
  String toString() {
    return '$GameTheme(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'code': code,
      'categories': categories,
    };
  }
}
