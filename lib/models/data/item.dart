class Item {
  final String key;
  final String text;

  const Item({
    required this.key,
    required this.text,
  });

  Map<String, dynamic> toJson() {
    return {
      'key': key,
      'text': text,
    };
  }

  @override
  String toString() {
    return toJson().toString();
  }
}
