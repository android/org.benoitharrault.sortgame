import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:sortgame/config/application_config.dart';

import 'package:sortgame/cubit/activity/activity_cubit.dart';
import 'package:sortgame/models/activity/activity.dart';

class PositionIndicator extends StatelessWidget {
  const PositionIndicator({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        // Normalized [0..1] value
        final double barValue = currentActivity.position /
            int.parse(currentActivity.activitySettings
                .get(ApplicationConfig.parameterCodeItemsCount));

        const Color baseColor = Color.fromARGB(255, 215, 1, 133);

        const barHeight = 40.0;
        const Color textColor = Color.fromARGB(255, 238, 238, 238);
        const Color outlineColor = Color.fromARGB(255, 200, 200, 200);

        return Stack(
          alignment: Alignment.center,
          children: [
            LinearProgressIndicator(
              value: barValue,
              color: baseColor,
              backgroundColor: baseColor.darken(),
              minHeight: barHeight,
              borderRadius: const BorderRadius.all(Radius.circular(barHeight / 4)),
            ),
            OutlinedText(
              text:
                  '${currentActivity.position}/${currentActivity.activitySettings.get(ApplicationConfig.parameterCodeItemsCount)}',
              fontSize: 0.9 * barHeight,
              textColor: textColor,
              outlineColor: outlineColor,
            ),
          ],
        );
      },
    );
  }
}
