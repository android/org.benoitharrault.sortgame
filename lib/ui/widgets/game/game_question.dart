import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sortgame/cubit/activity/activity_cubit.dart';
import 'package:sortgame/models/data/game_item.dart';
import 'package:sortgame/models/activity/activity.dart';
import 'package:sortgame/ui/widgets/game/buttons_yes_no.dart';

class GameQuestionWidget extends StatelessWidget {
  const GameQuestionWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final GameItem currentGameItem = currentActivity.getCurrentGameItem();

        return Column(
          children: [
            OutlinedText(
              text: currentGameItem.item.text,
              fontSize: 50,
              textColor: Theme.of(context).colorScheme.onSurface,
            ),
            Container(
              padding: const EdgeInsets.all(10),
              margin: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Theme.of(context).colorScheme.surface,
                  width: 8,
                ),
                borderRadius: const BorderRadius.all(Radius.circular(20)),
                color: Theme.of(context).colorScheme.inversePrimary,
              ),
              child: GameButtonsYesNo(gameItem: currentGameItem),
            ),
          ],
        );
      },
    );
  }
}
