import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sortgame/config/application_config.dart';
import 'package:sortgame/data/game_data.dart';
import 'package:sortgame/models/data/category.dart';
import 'package:sortgame/models/data/game_item.dart';
import 'package:sortgame/models/data/game_theme.dart';
import 'package:sortgame/models/data/item.dart';

class FetchDataHelper {
  FetchDataHelper();

  final List<Category> _categories = [];
  List<Category> get categories => _categories;

  final List<Item> _items = [];
  List<Item> get items => _items;

  final Map<String, GameTheme> _themes = {};
  Map<String, GameTheme> get themes => _themes;

  final List<GameItem> _mapping = [];

  Category getCategory(String code) {
    return _categories.firstWhere((category) => (category.key == code));
  }

  void init() {
    try {
      const gameData = GameData.data;

      final Map<String, String> emojis = {};
      final Map<String, dynamic> rawResources = gameData['resources'] as Map<String, dynamic>;
      final Map<String, String> rawEmojis = rawResources['categories'] as Map<String, String>;
      rawEmojis.forEach((categoryCode, emoji) {
        emojis[categoryCode] = emoji;
      });

      final List<dynamic> rawCategories = gameData['categories'] as List<dynamic>;
      for (var rawElement in rawCategories) {
        final categoryCode = rawElement.toString();
        _categories.add(Category(
          key: categoryCode,
          text: categoryCode,
          emoji: emojis[categoryCode] ?? '',
        ));
      }

      final Map<String, dynamic> rawThemes = gameData['themes'] as Map<String, dynamic>;
      rawThemes.forEach((code, rawCategories) {
        final List<Category> categories = [];
        for (var rawElement in rawCategories) {
          final category = getCategory(rawElement.toString());
          categories.add(category);
        }
        _themes[code] = GameTheme(code: code, categories: categories);
      });

      final List<dynamic> rawItems = gameData['items'] as List<dynamic>;
      for (var rawElement in rawItems) {
        final element = rawElement.toString();
        _items.add(Item(key: element, text: element));
      }

      final Map<String, dynamic> rawMapping = gameData['mapping'] as Map<String, dynamic>;
      final Map<String, dynamic> rawMappingItems = rawMapping['items'] as Map<String, dynamic>;
      rawMappingItems.forEach(
        (String itemName, itemMappings) {
          final List<String> rawIsCategories = [];
          for (var category in itemMappings['is'] as List<dynamic>) {
            rawIsCategories.add(category.toString());
          }

          final List<String> rawIsNotCategories = [];
          for (var category in itemMappings['isnot'] as List<dynamic>) {
            rawIsNotCategories.add(category.toString());
          }

          _mapping.add(GameItem(
            item: Item(
              key: itemName,
              text: itemName,
            ),
            isCategory: rawIsCategories.map((String code) => getCategory(code)).toList(),
            isNotCategory: rawIsNotCategories.map((String code) => getCategory(code)).toList(),
          ));
        },
      );
    } catch (e) {
      printlog("$e");
    }
  }

  List<GameItem> getItems(ActivitySettings activitySettings) {
    if (_mapping.isEmpty) {
      init();
    }

    final int count =
        int.parse(activitySettings.get(ApplicationConfig.parameterCodeItemsCount));
    final String themeCode = activitySettings.get(ApplicationConfig.parameterCodeThemeCode);

    List<GameItem> items = _mapping;

    // Remove unwanted categories if theme is selected
    final GameTheme gameTheme = getTheme(code: themeCode);
    if (gameTheme.categories.isNotEmpty) {
      for (GameItem item in items) {
        item.isCategory.removeWhere((Category category) =>
            (!gameTheme.categories.map((Category c) => c.key).contains(category.key)));
        item.isNotCategory.removeWhere((Category category) =>
            (!gameTheme.categories.map((Category c) => c.key).contains(category.key)));
      }
    }

    // Remove items without enough data
    items.removeWhere((GameItem gameItem) =>
        (gameItem.isCategory.isEmpty || gameItem.isNotCategory.isEmpty));

    items.shuffle();

    return items.take(count).toList();
  }

  List<String> getThemesCodes() {
    if (_themes.isEmpty) {
      init();
    }

    return _themes.keys.toList();
  }

  GameTheme getTheme({required String code}) {
    if (_themes.isEmpty) {
      init();
    }

    return _themes[code] ??
        GameTheme(
          code: '',
          categories: [],
        );
  }
}
